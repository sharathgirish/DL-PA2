# -*- coding: utf-8 -*-
import scrapy
from bs4 import BeautifulSoup
from pathlib import Path
home = str(Path.home())

class RedditbotSpider(scrapy.Spider):
    name = 'redditbot'
    allowed_domains = ['www.reddit.com/r/gameofthrones/']
    start_urls = ['http://www.reddit.com/r/gameofthrones//']

    def parse(self, response):
    	soup = BeautifulSoup(response.text,"lxml")
    	with open(home+'/dl/DL-PA2/crawlers/data.txt', 'a+') as f:
    		f.write(soup.get_text())