import re

f = open('data.txt', 'r')
a = []
for line in f.readlines():
    l = re.sub(r"[\x00-\x1f]+","", re.sub(r"[\x21-\x7f]+", "", line))
    a.append(l)

g = open('data.txt','w')
for l in a:
    #TODO: clean/trim the spaces
    g.write(l)
    g.write('\n')

g.close()
