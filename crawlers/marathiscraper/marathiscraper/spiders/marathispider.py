# -*- coding: utf-8 -*-
import scrapy
from bs4 import BeautifulSoup
from pathlib import Path
home = str(Path.home())

from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors import LinkExtractor
from scrapy.selector import HtmlXPathSelector

class MarathispiderSpider(CrawlSpider):
    name = "marathispider"
    #allowed_domains = ['http://mr.wikipedia.org/wiki/']
    #start_urls = ['http://mr.wikipedia.org/wiki/%E0%A4%AE%E0%A5%81%E0%A4%96%E0%A4%AA%E0%A5%83%E0%A4%B7%E0%A5%8D%E0%A4%A0']
    #start_urls = ['http://maharashtratimes.indiatimes.com/']
    #start_urls = ['https://maharashtratimes.indiatimes.com/maharashtra/nashik-north-maharashtra-news/dhule/articlelist/47583333.cms']
    #start_urls = ['http://www.lokmat.com/']
    start_urls = ['https://www.maayboli.com/']
    #category_list = ['/maharashtra','/india','/international','/business','/sports','/editorial','/entertainment','/career',\
    #'/lifestyle','/gadget','/jokes','/citizen']
    category_list = ['/node','/new4','/top20','/marathi','/hitguj','/gulmohar','/vishesh']
    count=0
    def parse(self, response):
    	soup = BeautifulSoup(response.text,"lxml")
    	with open(home+'/dl/DL-PA2/data/data_maayboli.txt', 'a+') as f:
    		f.write(soup.get_text())
    		self.count = self.count+1
    	next_page = response.xpath('//a[@class="hdrlnk"]/@href').extract()
    	links = response.xpath('//a/@href').extract()
    	for next_page in links:
    		if(next_page.startswith(tuple(self.category_list))):
		    	new_page = response.urljoin(next_page)
    			print("Count:"+str(self.count))
    			if(self.count<10000):
    				yield scrapy.Request(new_page, callback=self.parse)