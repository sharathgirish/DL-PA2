import re

filepath = '../data/mhtimes/data.txt'
tasklist = ['remove eng chars, new lines','rem duplicate lines','unique words']
switch = 3
if switch ==1:
	f = open(filepath, 'r')
	a = []
	for line in f.readlines():
	    l = re.sub(r"[\x00-\x1f]+","", re.sub(r"[\x21-\x7f]+", "", line))
	    a.append(l)
	f.close()

	g = open(filepath,'w')
	for l in a:
	    #TODO: clean/trim the spaces
	    if not re.match(r'^\s*$', l):
	    	g.write(l)
	    	g.write('\n')

	g.close()

if(switch==2):
	outfilepath = '../data/mhtimes/unique_lines.txt'
	s = set()
	with open(outfilepath, 'w') as out:
		for line in open(filepath,'r'):
			if line not in s:
				out.write(line)
				s.add(line)

if switch==3:
	filepath = '../data/mhtimes/unique_lines.txt'
	with open(filepath,'r') as f:
		file_contents = f.read()
		word_list = file_contents.split()
		unique_words = set(word_list)

	with open('../data/mhtimes/unique_words.txt','w') as f:
		for word in unique_words:
			f.write(str(word) + "\n")
