import logging

from nose.tools import assert_equal, assert_true
import numpy as np
from numpy.testing import assert_allclose

import sys
sys.path.insert(0,'/Users/apple/Desktop/glove.py') ####change this path

import evaluate
import glove

logging.getLogger().setLevel(logging.INFO)

# Mock corpus (shamelessly stolen from Gensim word2vec tests)
test_corpus = ("""human interface computer
survey user computer system response time
eps user interface system
system human system eps
user response time
trees
graph trees
graph minors trees
graph minors survey
I like graph and stuff
I like trees and stuff
Sometimes I build a graph
Sometimes I build trees""").split("\n")

glove.logger.setLevel(logging.ERROR)
vocab = glove.build_vocab(test_corpus)
cooccur = glove.build_cooccur(vocab, test_corpus, window_size=10)
id2word = evaluate.make_id2word(vocab)

W = glove.train_glove(vocab, cooccur, vector_size=10, iterations=500)

# Merge and normalize word vectors
W = evaluate.merge_main_context(W)


def test_similarity():
    similar = evaluate.most_similar(W, vocab, id2word, 'graph')
    logging.debug(similar)
    print (similar)
    # assert_equal('trees', similar[0])

test_similarity()


##Doesn't seem to be working!! :/

# # print(vocab[id2word[4]][1])
# def most_similar(positive, negative, topn=10, freq_threshold=5):
#     # Build a "mean" vector for the given positive and negative terms
#     mean_vecs = []
#     for word in positive: mean_vecs.append(W[vocab[word][0]])
#     for word in negative: mean_vecs.append(-1 * W[vocab[word][0]])
#
#     mean = np.array(mean_vecs).mean(axis=0)
#     mean /= np.linalg.norm(mean)
#
#     # Now calculate cosine distances between this mean vector and all others
#     dists = np.dot(W, mean)
#
#     best = np.argsort(dists)[::-1][:topn + len(positive) + len(negative) + 100]
#     result = [(id2word[i], dists[i]) for i in best if (vocab[id2word[i]][1] >= freq_threshold
#                                                        and id2word[i] not in positive
#                                                        and id2word[i] not in negative)]
#     return result[:topn]
#
# print(most_similar(['graph', 'like'], ['trees'], topn=1))
